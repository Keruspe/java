# Copyright 2017-2018 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

REVISION=""
BOOTSTRAP_VERSION="2.5.4"
BOOTSTRAP_ANT_VERSION="1.8.2"

SLOT="1.8"

require icedtea

PLATFORMS="~amd64 ~x86"

# The vast majority of tests succeed but still too many fail. See src_test in
# icedtea.exlib for details.
RESTRICT="test"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-Drop-bogus-toolchain-checks.patch
)

# DISTRIBUTION_PATCHES as used in the Makefile is *NOT* an array so we can't
# magically make it one here. It only worked by fluke so far because there only
# ever was *one* patch - till now.
DISTRIBUTION_PATCHES="patches/0001-Drop-bogus-toolchain-checks.patch"

src_prepare() {
    # This check comes too early, kill it.
    edo sed -i -e '/^IT_FIND_TOOLS.*FASTJAR/d' configure.ac

    autotools_src_prepare
}

src_configure() {
    # make icedtea build with gcc6+
    # see also https://patchwork.openembedded.org/patch/125471/
    # last checked: icedtea8-3.3.0
    append-flags "-fno-lifetime-dse -fno-delete-null-pointer-checks"
    append-cppflags "-std=gnu++98"
    icedtea_src_configure
}
